<?php

namespace App\Tests;

use App\Entity\Missions;
use PHPUnit\Framework\TestCase;

class MissionsUnitTest extends TestCase
{
    public function testisTrue(): void
    {
        $datetime = new \DateTime();
        $endDateTime = new \DateTime();
        $mission = new Missions();
        $mission->setNomCode('PainBeurre');
        $mission->setSpécialité('Infiltration');
        $mission->setType('Infiltration');
        $mission->setPays('Japon');
        $mission->setDescription('Une mission');
        $mission->setDateDebut($datetime);
        $mission->setDateFin($endDateTime);
        $mission->setStatut('en cours');



        $this->assertTrue($mission->getNomCode()==='PainBeurre');
        $this->assertTrue($mission->getSpécialité()==='Infiltration');
        $this->assertTrue($mission->getType()==='Infiltration');
        $this->assertTrue($mission->getPays()==='Japon');
        $this->assertTrue($mission->getDescription()==='Une mission');
        $this->assertTrue($mission->getDateDebut() === $datetime);
        $this->assertTrue($mission->getDateFin()===$endDateTime);
        $this->assertTrue($mission->getStatut()==='en cours');



    }
    public function testIsFalse() :void
    {
        $datetime = new \DateTime();
        $endDateTime = new \DateTime();
        $mission = new Missions();
        $mission->setNomCode('PainBeurre');
        $mission->setSpécialité('Infiltration');
        $mission->setType('Infiltration');
        $mission->setPays('Japon');
        $mission->setDescription('Une mission');
        $mission->setDateDebut($datetime);
        $mission->setDateFin($endDateTime);
        $mission->setStatut('en cours');


        $this->assertFalse($mission->getNomCode()==='Jambon');
        $this->assertFalse($mission->getSpécialité()==='Filature');
        $this->assertFalse($mission->getType()==='Filature');
        $this->assertFalse($mission->getPays()==='Inde');
        $this->assertFalse($mission->getDescription()==='Une planque');
        $this->assertFalse($mission->getDateDebut() === new \DateTime());
        $this->assertFalse($mission->getDateFin()===new \DateTime());
        $this->assertFalse($mission->getStatut()==='terminé');


    }
    public function testIsEmpty():void
    {
        $datetime = new \DateTime();
        $endDateTime = new \DateTime();
        $mission = new Missions();
        $mission->setNomCode('PainBeurre');
        $mission->setSpécialité('Infiltration');
        $mission->setType('Infiltration');
        $mission->setPays('Japon');
        $mission->setDescription('Une mission');
        $mission->setDateDebut($datetime);
        $mission->setDateFin($endDateTime);
        $mission->setStatut('en cours');


        $this->assertEmpty($mission->getNomCode()==='');
        $this->assertEmpty($mission->getSpécialité()==='');
        $this->assertEmpty($mission->getType()==='');
        $this->assertEmpty($mission->getPays()==='');
        $this->assertEmpty($mission->getDescription()==='');
        $this->assertEmpty($mission->getDateDebut() === '');
        $this->assertEmpty($mission->getDateFin()==='');
        $this->assertEmpty($mission->getStatut()==='');

    }
}
