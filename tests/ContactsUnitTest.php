<?php

namespace App\Tests;

use App\Entity\Contacts;
use PHPUnit\Framework\TestCase;

class ContactsUnitTest extends TestCase
{
    public function testisTrue(): void
    {
        $datetime = new \DateTime();
        $contact = new Contacts();
        $contact->setPrenom('Pierre');
        $contact->setDateNaissance($datetime);
        $contact->setNomCode("Rat");
        $contact->setNom('Bon');
        $contact->setNationalité('Japonaise');



        $this->assertTrue($contact->getDateNaissance() === $datetime);
        $this->assertTrue($contact->getNationalité() === 'Japonaise');
        $this->assertTrue($contact->getPrenom() === 'Pierre');
        $this->assertTrue($contact->getNom() === 'Bon');
        $this->assertTrue($contact->getNomCode() === 'Rat');


    }
    public function testIsFalse() :void
    {
        $datetime = new \DateTime();
        $contact = new Contacts();
        $contact->setPrenom('Pierre');
        $contact->setDateNaissance($datetime);
        $contact->setNomCode("Rat");
        $contact->setNom('Bon');
        $contact->setNationalité('Japonaise');


        $this->assertFalse($contact->getDateNaissance() === new \DateTime());
        $this->assertFalse($contact->getNationalité() === 'Hongroise');
        $this->assertFalse($contact->getNom() === 'PasBon');
        $this->assertFalse($contact->getprenom() === 'Jeanne');
        $this->assertFalse($contact->getNomCode() === 'Lapin');


    }
    public function testIsEmpty():void
    {
        $datetime = new \DateTime();
        $contact = new Contacts();
        $contact->setPrenom('Pierre');
        $contact->setDateNaissance($datetime);
        $contact->setNomCode("Rat");
        $contact->setNom('Bon');
        $contact->setNationalité('Japonaise');


        $this->assertEmpty($contact->getDateNaissance()=== '');
        $this->assertEmpty($contact->getPrenom() === '');
        $this->assertEmpty($contact->getNom() === '');
        $this->assertEmpty($contact->getNomCode() === '');
        $this->assertEmpty($contact->getNationalité()=== '');

    }
}
