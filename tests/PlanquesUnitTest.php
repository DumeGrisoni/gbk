<?php

namespace App\Tests;

use App\Entity\Planques;
use PHPUnit\Framework\TestCase;

class PlanquesUnitTest extends TestCase
{
    public function testisTrue(): void
    {
        $planque = new Planques();
        $planque->setCodePlanque(010101);
        $planque->setPays('France');
        $planque->setType("Maisonette");
        $planque->setAdresse('2 rue planque');




        $this->assertTrue($planque->getCodePlanque() === 010101);
        $this->assertTrue($planque->getPays() === 'France');
        $this->assertTrue($planque->getType() === 'Maisonette');
        $this->assertTrue($planque->getAdresse() === '2 rue planque');



    }
    public function testIsFalse() :void
    {
        $planque = new Planques();
        $planque->setCodePlanque(010101);
        $planque->setPays('France');
        $planque->setType("Maisonette");
        $planque->setAdresse(' 2 rue planque');


        $this->assertFalse($planque->getCodePlanque() === 020202);
        $this->assertFalse($planque->getPays() === 'Inde');
        $this->assertFalse($planque->getType() === 'Studio');
        $this->assertFalse($planque->getAdresse() === 'avenue de studio');


    }
    public function testIsEmpty():void
    {
        $planque = new Planques();
        $planque->setCodePlanque(010101);
        $planque->setPays('France');
        $planque->setType("Maisonette");
        $planque->setAdresse(' 2 rue planque');


        $this->assertEmpty($planque->getCodePlanque() === '');
        $this->assertEmpty($planque->getPays() === '');
        $this->assertEmpty($planque->getType() === '');
        $this->assertEmpty($planque->getAdresse() === '2 rue ');

    }
}
