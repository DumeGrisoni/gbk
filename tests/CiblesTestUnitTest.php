<?php

namespace App\Tests;

use App\Entity\Cibles;
use PHPUnit\Framework\TestCase;

class CiblesTestUnitTest extends TestCase
{
    public function testisTrue(): void
    {
        $datetime = new \DateTime();
        $cible = new Cibles();
        $cible->setPrenom('Pierre');
        $cible->setDateNaissance($datetime);
        $cible->setNomCode("Rat");
        $cible->setNom('Bon');
        $cible->setNationalite('Japonaise');



        $this->assertTrue($cible->getDateNaissance() === $datetime);
        $this->assertTrue($cible->getNationalite() === 'Japonaise');
        $this->assertTrue($cible->getPrenom() === 'Pierre');
        $this->assertTrue($cible->getNom() === 'Bon');
        $this->assertTrue($cible->getNomCode() === 'Rat');


    }
    public function testIsFalse() :void
    {
        $datetime = new \DateTime();
        $cible = new Cibles();
        $cible->setPrenom('Pierre');
        $cible->setDateNaissance($datetime);
        $cible->setNomCode("Rat");
        $cible->setNom('Bon');
        $cible->setNationalite('Japonaise');


        $this->assertFalse($cible->getDateNaissance() === new \DateTime());
        $this->assertFalse($cible->getNationalite() === 'Hongroise');
        $this->assertFalse($cible->getNom() === 'PasBon');
        $this->assertFalse($cible->getprenom() === 'Jeanne');
        $this->assertFalse($cible->getNomCode() === 'Lapin');


    }
    public function testIsEmpty():void
    {
        $datetime = new \DateTime();
        $cible = new Cibles();
        $cible->setPrenom('Pierre');
        $cible->setDateNaissance($datetime);
        $cible->setNomCode("Rat");
        $cible->setNom('Bon');
        $cible->setNationalite('Japonaise');


        $this->assertEmpty($cible->getDateNaissance()=== '');
        $this->assertEmpty($cible->getPrenom() === '');
        $this->assertEmpty($cible->getNom() === '');
        $this->assertEmpty($cible->getNomCode() === '');
        $this->assertEmpty($cible->getNationalite()=== '');

    }
}
