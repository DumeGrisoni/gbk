<?php

namespace App\Tests;

use App\Entity\Agents;
use PHPUnit\Framework\TestCase;

class AgentsUnitTest extends TestCase
{
    public function testisTrue(): void
    {
        $datetime = new \DateTime();
        $agent = new Agents();
        $agent->setPrenom('Pierre');
        $agent->setDateNaissance($datetime);
        $agent->setSpécialité("Infiltration");
        $agent->setNom('Bon');
        $agent->setNationalité('Française');
        $agent->setCodeAgent(010504);


        $this->assertTrue($agent->getDateNaissance() === $datetime);
        $this->assertTrue($agent->getNationalité() === 'Française');
        $this->assertTrue($agent->getPrenom() === 'Pierre');
        $this->assertTrue($agent->getNom() === 'Bon');
        $this->assertTrue($agent->getSpécialité()==='Infiltration');
        $this->assertTrue($agent->getCodeAgent() === 010504);

    }
    public function testIsFalse() :void
    {
        $datetime = new \DateTime();
        $agent = new Agents();
        $agent->setPrenom('Pierre');
        $agent->setDateNaissance($datetime);
        $agent->setSpécialité("Infiltration");
        $agent->setNom('Bon');
        $agent->setNationalité('Française');
        $agent->setCodeAgent(010504);

        $this->assertFalse($agent->getDateNaissance() === new \DateTime());
        $this->assertFalse($agent->getNationalité() === 'Hongroise');
        $this->assertFalse($agent->getNom() === 'PasBon');
        $this->assertFalse($agent->getprenom() === 'Jeanne');
        $this->assertFalse($agent->getCodeAgent() === 000000);
        $this->assertFalse($agent->getSpécialité() === 'Torture');

    }
    public function testIsEmpty():void
    {
        $datetime = new \DateTime();
        $agent = new Agents();
        $agent->setPrenom('Pierre');
        $agent->setDateNaissance($datetime);
        $agent->setSpécialité("Infiltration");
        $agent->setNom('Bon');
        $agent->setNationalité('Française');
        $agent->setCodeAgent(010504);

        $this->assertEmpty($agent->getDateNaissance()=== '');
        $this->assertEmpty($agent->getPrenom() === '');
        $this->assertEmpty($agent->getNom() === '');
        $this->assertEmpty($agent->getCodeAgent() === '');
        $this->assertEmpty($agent->getSpécialité()=== '');
        $this->assertEmpty($agent->getNationalité()=== '');
    }
}
