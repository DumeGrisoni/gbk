<?php

namespace App\Tests;

use App\Entity\Administrateur;
use DateTime;
use PHPUnit\Framework\TestCase;

class AdminUnitTest extends TestCase
{
    public function testisTrue(): void
    {
        $datetime = new DateTime();
        $admin = new Administrateur();
        $admin->setPrenom('Pierre');
        $admin->setDateCreation($datetime);
        $admin->setPassword('password');
        $admin->setNom('Bon');
        $admin->setEmail('mon@email.com');


        $this->assertTrue($admin->getDateCreation() === $datetime);
        $this->assertTrue($admin->getEmail() === 'mon@email.com');
        $this->assertTrue($admin->getPrenom() === 'Pierre');
        $this->assertTrue($admin->getNom() === 'Bon');
        $this->assertTrue($admin->getPassword()==='password');
    }
    public function testIsFalse() :void
    {
        $datetime = new DateTime();
        $admin = new Administrateur();
        $admin->setPrenom('Pierre');
        $admin->setDateCreation($datetime);
        $admin->setPassword('password');
        $admin->setNom('Bon');
        $admin->setEmail('mon@email.com');

        $this->assertFalse($admin->getDateCreation() === new DateTime());
        $this->assertFalse($admin->getPassword() === 'Bad password');
        $this->assertFalse($admin->getNom() === 'PasBon');
        $this->assertFalse($admin->getprenom() === 'Jeanne');
        $this->assertFalse($admin->getEmail() === 'pasmon@email.com');

    }
    public function testIsEmpty():void
    {
        $datetime = new DateTime();
        $admin = new Administrateur();
        $admin->setPrenom('Pierre');
        $admin->setDateCreation($datetime);
        $admin->setPassword('password');
        $admin->setNom('Bon');
        $admin->setEmail('mon@email.com');

        $this->assertEmpty($admin->getDateCreation()=== '');
        $this->assertEmpty($admin->getPrenom() === '');
        $this->assertEmpty($admin->getNom() === '');
        $this->assertEmpty($admin->getPassword() === '');
        $this->assertEmpty($admin->getEmail()=== '');
    }

}