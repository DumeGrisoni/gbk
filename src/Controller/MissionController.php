<?php

namespace App\Controller;

use App\Entity\Agents;
use App\Entity\Cibles;
use App\Entity\Missions;
use App\Entity\Planques;
use App\Entity\Contacts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MissionController extends AbstractController
{
    /**
     * @return Response
     * @Route ("/mission/{id}", name="mission.index")
     */
    public function index(string $id): Response

    {// Recuperation de l'Entity Manager
        $entity = $this->getDoctrine()->getManager();

        //Recuperer le Repository des Missions
        $mission = $entity->getRepository(Missions::class)->findOneBy(['id' => $id]);
        $agent = $mission->getAgent();

        return $this->render('mission/index.html.twig', [
            'mission' => $mission,
            'agent' => $agent
        ]);
    }

}
