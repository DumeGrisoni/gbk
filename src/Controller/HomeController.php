<?php

namespace App\Controller;

use App\Entity\Missions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * Page d'Accueil
     * @Route("/", name="Home")
     * @return Response
     */
    public function index(): Response
    {
        // Recuperation de l'Entity Manager
        $entity = $this->getDoctrine()->getManager();

        //Recuperer le Repository des Missions et ses données
        $mission = $entity->getRepository(Missions::class)->findAll();

        return $this->render('home/Home.html.twig', [
            'missions' => $mission
        ]);
    }
}
