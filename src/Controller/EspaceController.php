<?php

namespace App\Controller;



use App\Entity\Agents;
use App\Entity\Cibles;
use App\Entity\Contacts;
use App\Entity\Missions;
use App\Entity\Planques;
use App\Form\AgentType;
use App\Form\CiblesType;
use App\Form\ContactType;
use App\Form\MissionType;
use App\Form\PlanqueType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class EspaceController extends AbstractController
{

    // DISPLAY DES ACTEURS
    /**
     * @Route ("/espace", name="espace.index")
     */
    public function index() :Response
    {
        $mission = $this->getDoctrine()->getRepository(Missions::class)->findAll();
        $agent = $this->getDoctrine()->getRepository(Agents::class)->findAll();
        $planque= $this->getDoctrine()->getRepository(Planques::class)->findAll();
        $contact = $this->getDoctrine()->getRepository(Contacts::class)->findAll();
        $cible = $this->getDoctrine()->getRepository(Cibles::class)->findAll();
        return $this->render('espace/index.html.twig',[
            'menu_active' => 'active',
            'missions' => $mission,
            'agents' => $agent,
            'cibles' => $cible,
            'planques' => $planque,
            'contacts' => $contact

        ]);
    }

    // Création du CRUD pour les missions

    //CREATION d'une mission
    /**
     * @Route("/espace/mission/create", name="mission.create")
     */
    public function new(Request $request)
    {

        $mission = new Missions();
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(MissionType::class, $mission);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
                $entityManager->persist($mission);
                $entityManager->flush();
                $this->addFlash('success', 'Mission bien créée');
                return $this->redirectToRoute('espace.index');
        }
        return $this->render('espace/mission/new.html.twig', [
            'menu_active' => 'active',
            'missions' => $mission,
            'form' => $form->createView()
        ]);
    }

    // SUPPRESSION d'une mission
    /**
     * @param Missions $mission
     * @param Request $request
     * @return Response
     * @Route ("/espace/missions/delete/{id}", name="mission.delete", methods={"DELETE", "POST"})
     */
    public function delete(Missions $mission, Request $request):Response
    {
        if ($this->isCsrfTokenValid('delete'.$mission->getId(), $request->get('_token'))){

            $em = $this->getDoctrine()->getManager();
            $em->remove($mission);
            $em->flush();
            $this->addFlash('success', 'Mission bien supprimée');
        };

        return $this->redirectToRoute('espace.index');
    }

    //EDITION d'une mission
    /**
     * @Route ("/espace/mission/{id}", name="mission.edit", methods={"POST","GET"})
     * @param Missions $missions
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function edit(Missions $missions,Request $request,) : Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(MissionType::class, $missions);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
                $entityManager->flush();
                $this->addFlash('success', 'Mission bien modifiée');
                return $this->redirectToRoute('espace.index');
        }


        return $this->render('espace/mission/edit.html.twig', [
            'menu_active' => 'active',
            'missions' => $missions,
            'form' => $form->createView()
        ]);
    }


    // Création du CRUD pour les agents


    // CREATION d'un agent
    /**
     * @Route("/espace/agent/create", name="agent.create")
     */
    public function newAgent(Request $request)
    {
        $agent = new Agents();
        $manager = $this->getDoctrine()->getManager();

        $form = $this->createForm(AgentType::class, $agent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($agent);
            $manager->flush();
            $this->addFlash('success', 'Agent bien créé');
            return $this->redirectToRoute('espace.index');
        }
        return $this->render('espace/agent/new.html.twig', [
            'menu_active' => 'active',
            'agents' => $agent,
            'form' => $form->createView()
        ]);
    }

    // EDITION d'un agent
    /**
     * @Route ("/espace/agent/{id}", name="agent.edit", methods={"POST","GET"})
     * @param Agents $agents
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editAgent(Agents $agents,Request $request) : Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(AgentType::class, $agents);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager->flush();
            $this->addFlash('success', 'Agent bien modifié');
            return $this->redirectToRoute('espace.index');
        }


        return $this->render('espace/agent/edit.html.twig', [
            'menu_active' => 'active',
            'agents' => $agents,
            'form' => $form->createView()
        ]);
    }

    //SUPPRESSION d'un agent
    /**
     * @param Agents $agents
     * @param Request $request
     * @return Response
     * @Route ("/espace/agent/delete/{id}", name="agent.delete", methods={"DELETE", "POST"})
     */
    public function deleteAgent(Agents $agents, Request $request):Response
    {
        if ($this->isCsrfTokenValid('delete'.$agents->getId(), $request->get('_token'))){

            $em = $this->getDoctrine()->getManager();
            $em->remove($agents);
            $em->flush();
            $this->addFlash('success', 'Agent bien supprimé');
        };

        return $this->redirectToRoute('espace.index');
    }


    // Création du CRUD pour les cibles


    //CREATION d'une cible
    /**
     * @Route("/espace/cible/create", name="cible.create")
     */
    public function newCible(Request $request)
    {
        $cibles = new Cibles();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CiblesType::class, $cibles);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($cibles);
            $em->flush();
            $this->addFlash('success', 'Cible bien créée');
            return $this->redirectToRoute('espace.index');
        }
        return $this->render('espace/cible/new.html.twig', [
            'menu_active' => 'active',
            'cibles' => $cibles,
            'form' => $form->createView()
        ]);
    }


    // EDITION d'une cible

    /**
     * @Route ("/espace/cible/{id}", name="cible.edit", methods={"POST","GET"})
     * @param Cibles $cibles
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editCibles(Cibles $cibles,Request $request) : Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(CiblesType::class, $cibles);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager->flush();
            $this->addFlash('success', 'Cible bien modifiée');
            return $this->redirectToRoute('espace.index');
        }


        return $this->render('espace/cible/edit.html.twig', [
            'menu_active' => 'active',
            'cibles' => $cibles,
            'form' => $form->createView()
        ]);
    }


    //SUPPRESSION d'une cible

    /**
     * @param Cibles $cibles
     * @param Request $request
     * @return Response
     * @Route ("/espace/cible/delete/{id}", name="cible.delete", methods={"DELETE", "POST"})
     */
    public function deleteCible(Cibles $cibles, Request $request):Response
    {
        if ($this->isCsrfTokenValid('delete'.$cibles->getId(), $request->get('_token'))){

            $em = $this->getDoctrine()->getManager();
            $em->remove($cibles);
            $em->flush();
            $this->addFlash('success', 'Cible bien supprimée');
        };

        return $this->redirectToRoute('espace.index');
    }


    // Création du CRUD pour les Contacts


    //CREATION d'un contact
    /**
     * @Route("/espace/contact/create", name="contact.create")
     */
    public function newContact(Request $request)
    {
        $contacts = new Contacts();
        $entity= $this->getDoctrine()->getManager();

        $form = $this->createForm(ContactType::class, $contacts);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->persist($contacts);
            $entity->flush();
            $this->addFlash('success', 'Contact bien créé');
            return $this->redirectToRoute('espace.index');
        }
        return $this->render('espace/contact/new.html.twig', [
            'menu_active' => 'active',
            'contacts' => $contacts,
            'form' => $form->createView()
        ]);
    }


    // EDITION d'un contact

    /**
     * @Route ("/espace/contact/{id}", name="contact.edit", methods={"POST","GET"})
     * @param Contacts $contacts
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editContact(Contacts $contacts,Request $request) : Response
    {
        $entity= $this->getDoctrine()->getManager();
        $form = $this->createForm(ContactType::class, $contacts);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entity->flush();
            $this->addFlash('success', 'Contact bien modifié');
            return $this->redirectToRoute('espace.index');
        }


        return $this->render('espace/contact/edit.html.twig', [
            'menu_active' => 'active',
            'contacts' => $contacts,
            'form' => $form->createView()
        ]);
    }


    //SUPPRESSION d'une cible

    /**
     * @param Contacts $contacts
     * @param Request $request
     * @return Response
     * @Route ("/espace/contact/delete/{id}", name="contact.delete", methods={"DELETE", "POST"})
     */
    public function deleteContact(Contacts $contacts, Request $request):Response
    {
        if ($this->isCsrfTokenValid('delete'.$contacts->getId(), $request->get('_token'))){

            $em = $this->getDoctrine()->getManager();
            $em->remove($contacts);
            $em->flush();
            $this->addFlash('success', 'Contact bien supprimé');
        };

        return $this->redirectToRoute('espace.index');
    }


    // Création du CRUD pour les Planques


    //CREATION d'une planque
    /**
     * @Route("/espace/planque/create", name="planque.create")
     */
    public function newPlanque(Request $request)
    {
        $planques = new Planques();
        $entity= $this->getDoctrine()->getManager();

        $form = $this->createForm(PlanqueType::class, $planques);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->persist($planques);
            $entity->flush();
            $this->addFlash('success', 'Planque bien créée');
            return $this->redirectToRoute('espace.index');
        }
        return $this->render('espace/planque/new.html.twig', [
            'menu_active' => 'active',
            'planques' => $planques,
            'form' => $form->createView()
        ]);
    }


    // EDITION d'une Planque

    /**
     * @Route ("/espace/planque/{id}", name="planque.edit", methods={"POST","GET"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editPlanque(Planques $planques,Request $request) : Response
    {
        $entity= $this->getDoctrine()->getManager();
        $form = $this->createForm(PlanqueType::class, $planques);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entity->flush();
            $this->addFlash('success', 'Planque bien modifiée');
            return $this->redirectToRoute('espace.index');
        }


        return $this->render('espace/planque/edit.html.twig', [
            'menu_active' => 'active',
            'planques' => $planques,
            'form' => $form->createView()
        ]);
    }

    //SUPPRESSION d'une planque

    /**
     * @param Planques $planques
     * @param Request $request
     * @return Response
     * @Route ("/espace/planque/delete/{id}", name="planque.delete", methods={"DELETE", "POST"})
     */
    public function deletePlanque(Planques $planques, Request $request):Response
    {
        if ($this->isCsrfTokenValid('delete'.$planques->getId(), $request->get('_token'))){

            $em = $this->getDoctrine()->getManager();
            $em->remove($planques);
            $em->flush();
            $this->addFlash('success', 'Planque bien supprimée');
        };

        return $this->redirectToRoute('espace.index');
    }


}
