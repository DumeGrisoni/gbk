<?php

namespace App\Entity;

use App\Repository\AdministrateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=AdministrateurRepository::class)
 */
class Administrateur implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_creation;

    /**
     * @ORM\ManyToMany(targetEntity=Missions::class)
     */
    private $mission;

    /**
     * @ORM\ManyToMany(targetEntity=Agents::class)
     */
    private $agent;

    /**
     * @ORM\ManyToMany(targetEntity=Cibles::class)
     */
    private $cible;

    /**
     * @ORM\ManyToMany(targetEntity=Contacts::class)
     */
    private $contact;

    /**
     * @ORM\ManyToMany(targetEntity=Planques::class)
     */
    private $planque;

    public function __construct()
    {
        $this->mission = new ArrayCollection();
        $this->agent = new ArrayCollection();
        $this->cible = new ArrayCollection();
        $this->contact = new ArrayCollection();
        $this->planque = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * @return Collection|Missions[]
     */
    public function getMission(): Collection
    {
        return $this->mission;
    }

    public function addMission(Missions $mission): self
    {
        if (!$this->mission->contains($mission)) {
            $this->mission[] = $mission;
        }

        return $this;
    }

    public function removeMission(Missions $mission): self
    {
        $this->mission->removeElement($mission);

        return $this;
    }

    /**
     * @return Collection|Agents[]
     */
    public function getAgent(): Collection
    {
        return $this->agent;
    }

    public function addAgent(Agents $agent): self
    {
        if (!$this->agent->contains($agent)) {
            $this->agent[] = $agent;
        }

        return $this;
    }

    public function removeAgent(Agents $agent): self
    {
        $this->agent->removeElement($agent);

        return $this;
    }

    /**
     * @return Collection|Cibles[]
     */
    public function getCible(): Collection
    {
        return $this->cible;
    }

    public function addCible(Cibles $cible): self
    {
        if (!$this->cible->contains($cible)) {
            $this->cible[] = $cible;
        }

        return $this;
    }

    public function removeCible(Cibles $cible): self
    {
        $this->cible->removeElement($cible);

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContact(): Collection
    {
        return $this->contact;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contact->contains($contact)) {
            $this->contact[] = $contact;
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        $this->contact->removeElement($contact);

        return $this;
    }

    /**
     * @return Collection|Planques[]
     */
    public function getPlanque(): Collection
    {
        return $this->planque;
    }

    public function addPlanque(Planques $planque): self
    {
        if (!$this->planque->contains($planque)) {
            $this->planque[] = $planque;
        }

        return $this;
    }

    public function removePlanque(Planques $planque): self
    {
        $this->planque->removeElement($planque);

        return $this;
    }
}
