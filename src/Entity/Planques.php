<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlanquesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PlanquesRepository::class)
 */

class Planques
{

    public function __toString()
    {
        return ' Code: '.$this->code_planque.' - '.$this->type.' Adresse : '.$this->adresse.' Pays: '.$this->pays;

    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank (message="Ce champs ne peut être vide")
     * @Assert\Type(type="integer", message="Ce champs doit être une valeur numérique")
     * @ORM\Column(type="integer")
     */
    private $code_planque;

    /**
     * @Assert\NotBlank (message="Ce champs ne peut être vide")
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @Assert\NotBlank (message="Ce champs ne peut être vide")
     * @ORM\Column(type="string", length=255)
     */
    private $pays;

    /**
     * @Assert\NotBlank (message="Ce champs ne peut être vide")
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Missions::class, inversedBy="planque")
     */
    private $missions;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodePlanque(): ?int
    {
        return $this->code_planque;
    }

    public function setCodePlanque(int $code_planque): self
    {
        $this->code_planque = $code_planque;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMissions(): ?Missions
    {
        return $this->missions;
    }

    public function setMissions(?Missions $missions): self
    {
        $this->missions = $missions;

        return $this;
    }

    const PLANQUETYPE = [
        'Studio' => 'Studio',
        'Maison' => 'Maison',
        'Appartement' => 'Appartement',
        'Hotel' => 'Hotel'
    ];

}
