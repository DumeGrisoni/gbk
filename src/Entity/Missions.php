<?php

namespace App\Entity;


use App\Repository\MissionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


/**
 * @ORM\Entity(repositoryClass=MissionsRepository::class)
 *
 */
class Missions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pays;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_debut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_fin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $specialite;


    /**
     * @ORM\OneToMany(targetEntity=Agents::class, mappedBy="missions", cascade={"persist"})
     */
    private $agent;

    /**
     * @ORM\OneToMany(targetEntity=Planques::class, mappedBy="missions")
     */
    private $planque;

    /**
     * @ORM\OneToMany(targetEntity=Contacts::class, mappedBy="missions")
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity=Cibles::class, mappedBy="missions")
     */
    private $cible;

    public function __construct()
    {
        $this->agent = new ArrayCollection();
        $this->planque = new ArrayCollection();
        $this->contact = new ArrayCollection();
        $this->cible = new ArrayCollection();
    }

    const TYPE = [
        'Infiltration' => 'Infiltration',
        'Assassinat' =>'Assassinat',
        'Diplomatie'=> 'Diplomatie',
        'Protection' =>'Protection'
    ];

    const SPEC = [
        'Explosifs' =>'Explosifs',
        'Relations humaines'=> 'Relations humaines',
        'Poison' => 'Poison',
        'Couteau' => 'Couteau',
        'Armes lourdes' => 'Armes lourdes',
        'Art de la discretion' => 'Art de la discretion'
    ];

    const STATUT = [
        'En préparation' => 'En préparation',
        'En cours'=> 'En cours',
        'Terminé' => 'Terminé'
    ];

    const PAYS = [
        'Japon' =>'Japon',
        'France' => 'France',
        'Angleterre' => 'Angleterre',
        'Ireland' => 'Ireland',
        'United States of America' => 'United States of America',
        'Russie' => 'Russie',
        'Pologne'=> 'Pologne',
        'Pays-Bas'=>'Pays-bas',
        'Chine'=>'Chine',
        'Mongolie'=>'Mongolie'
    ];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCode(): ?string
    {
        return $this->nom_code;
    }

    public function setNomCode(string $nom_code): self
    {
        $this->nom_code = $nom_code;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getSpecialite(): ?string
    {
        return $this->specialite;
    }

    public function setSpecialite(string $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    /**
     * @return Collection|Agents[]
     */
    public function getAgent(): Collection
    {
        return $this->agent;
    }

    public function addAgent(Agents $agent): self
    {
        if (!$this->agent->contains($agent)) {
            $this->agent[] = $agent;
            $agent->setMissions($this);
        }

        return $this;
    }

    public function removeAgent(Agents $agent): self
    {
        if ($this->agent->removeElement($agent)) {
            // set the owning side to null (unless already changed)
            if ($agent->getMissions() === $this) {
                $agent->setMissions(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Planques[]
     */
    public function getPlanque(): Collection
    {
        return $this->planque;
    }

    public function addPlanque(Planques $planque): self
    {
        if (!$this->planque->contains($planque)) {
            $this->planque[] = $planque;
            $planque->setMissions($this);
        }

        return $this;
    }

    public function removePlanque(Planques $planque): self
    {
        if ($this->planque->removeElement($planque)) {
            // set the owning side to null (unless already changed)
            if ($planque->getMissions() === $this) {
                $planque->setMissions(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContact(): Collection
    {
        return $this->contact;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contact->contains($contact)) {
            $this->contact[] = $contact;
            $contact->setMissions($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contact->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getMissions() === $this) {
                $contact->setMissions(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cibles[]
     */
    public function getCible(): Collection
    {
        return $this->cible;
    }

    public function addCible(Cibles $cible): self
    {
        if (!$this->cible->contains($cible)) {
            $this->cible[] = $cible;
            $cible->setMissions($this);
        }

        return $this;
    }

    public function removeCible(Cibles $cible): self
    {
        if ($this->cible->removeElement($cible)) {
            // set the owning side to null (unless already changed)
            if ($cible->getMissions() === $this) {
                $cible->setMissions(null);
            }
        }

        return $this;
    }
    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context)
    {
        foreach ($this->getAgent() as $agent) {
            foreach ($this->getCible() as $cible){
                foreach ($this->getContact() as $contact){
                    foreach ($this->getPlanque() as  $planque){
                        if ($planque->getPays() != $this->pays){
                            $context->buildViolation('La planque doit être dans le même pays que la mission')
                                ->atPath('contact')
                                ->addViolation();
                        }
                    }
                    if ($contact->getNationalite() != $this->pays){
                        $context->buildViolation('le contact doit être du même pays que celui de la mission')
                            ->atPath('contact')
                            ->addViolation();
                    }
                }
                if ($cible->getNationalite() == $agent->getNationalite()){
                    $context->buildViolation('l\'Agent ne doit pas avoir la même nationalité que la cible')
                        ->atPath('cible')
                        ->addViolation();
                }
                if ($agent->getSpecialite() != $this->specialite ){
                    $context->buildViolation('l\'Agent doit avoir la même spécialité que la mission')
                        ->atPath('agent')
                        ->addViolation();
                }
            }

        }
    }




}
