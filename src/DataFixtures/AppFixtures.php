<?php

namespace App\DataFixtures;

use App\Entity\Administrateur;
use App\Entity\Agents;
use App\Entity\Cibles;
use App\Entity\Contacts;
use App\Entity\Missions;
use App\Entity\Planques;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
        {
            $this->passwordHasher = $passwordHasher;
        }
    public function load(ObjectManager $manager)
    {
        $date = new \DateTime(date('11-01-1993'));
        $administrateur = new Administrateur();
        $administrateur->setPrenom('Ad')
            ->setNom('Min')
            ->setEmail('Admin@admin.com')
            ->setPassword($this->passwordHasher->hashPassword($administrateur, 'Administrateur'))
            ->setDateCreation($date)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($administrateur);
        $manager->flush();

        $date = new \DateTime(date('24-05-1997'));
        $admin = new Administrateur();
        $admin->setPrenom('Robert')
            ->setNom('LeSecond')
            ->setEmail('Robert@admin.com')
            ->setPassword($this->passwordHasher->hashPassword($admin, 'MotDePasseAdmin'))
            ->setDateCreation($date)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $manager->flush();
    }

//    public function load(ObjectManager $manager)
//    {
//        $date = new \DateTime(date('28-11-1994'));
//         $agent = new  Agents();
//         $agent->setNationalité('Espagnol')
//             ->setNom('Jamon')
//             ->setPrenom('Edouardo')
//             ->setSpécialité('Infiltration')
//             ->setDateNaissance($date)
//             ->setCodeAgent(01050302);
//        $manager->persist($agent);
//
//        $manager->flush();
//    }
}
