<?php

namespace App\Form;

use App\Entity\Cibles;
use App\Entity\Missions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CiblesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('nationalite', ChoiceType::class,[
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'label' => 'Lieu de naissance',
                'choices' => Missions::PAYS
            ])
            ->add('nomCode')
            ->add('dateNaissance',DateTimeType::class, [
                'invalid_message' => 'Le champs doit correspondre à une date valide',
                'label' => 'Date de naissance',
                'date_format' => 'dMy',
                'years' => range(date('Y') -70, date('Y'))
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cibles::class,
        ]);
    }
}
