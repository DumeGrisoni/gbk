<?php

namespace App\Form;

use App\Entity\Agents;
use App\Entity\Cibles;
use App\Entity\Contacts;
use App\Entity\Missions;
use App\Entity\Planques;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('nom_code', null,[
                'label' => 'Nom'
            ])
            ->add('pays', ChoiceType::class, [
                'choices' => Missions::PAYS,
                'invalid_message' => 'Le champs doit correspondre à un des choix'

            ])
            ->add('description')
            ->add('type', ChoiceType::class, [
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'choices' => Missions::TYPE
            ])
            ->add('date_fin', DateTimeType::class, [
                'invalid_message' => 'Le champs doit correspondre à une date',
                'date_format' => 'dMy',
                'years' => range(date('Y'), date('Y') + 20)
            ])

            ->add('statut',ChoiceType::class, [
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'choices' => Missions::STATUT
            ])

            ->add('dateDebut', DateTimeType::class, [
                'invalid_message' => 'Le champs doit correspondre à une date',
                'date_format' => 'dMy',
                'years' => range(date('Y'), date('Y') + 20)
            ])

            ->add('agent', EntityType::class, [
                'multiple' => true,
               'expanded'=> true,
               'class' => Agents::class,
                'label' => 'Agents'

            ])
            ->add('planque', EntityType::class,[
                'multiple' => true,
                'expanded' =>true,
                'class'=>Planques::class,
                'label' => 'Planques'
            ])
            ->add('contact', EntityType::class,[
                'multiple' => true,
                'expanded' =>true,
                'class'=>Contacts::class,
                'label' => 'Contacts'
            ])
            ->add('cible', EntityType::class,[
                'multiple' => true,
                'expanded' =>true,
                'class'=> Cibles::class,
                'label' => 'Cible'
            ])
            ->add('specialite', ChoiceType::class, [
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'choices' => Missions::SPEC

            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Missions::class,
        ]);
    }

}
