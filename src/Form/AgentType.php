<?php

namespace App\Form;

use App\Entity\Agents;
use App\Entity\Missions;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('specialite',ChoiceType::class,[
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'choices' => Missions::SPEC
            ])
            ->add('codeAgent')
            ->add('nationalite', ChoiceType::class,[
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'label' => 'Lieu de naissance',
                'choices' => Missions::PAYS
            ])
            ->add('dateNaissance',DateTimeType::class, [
                'invalid_message' => 'Le champs doit correspondre à une date',
                'label' => 'Date de naissance',
                'date_format' => 'dMy',
                 'years' => range(date('Y') - 70, date('Y'))
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Agents::class,
        ]);
    }

}
