<?php

namespace App\Form;

use App\Entity\Missions;
use App\Entity\Planques;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code_planque')
            ->add('adresse')
            ->add('pays', ChoiceType::class,[
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'choices' => Missions::PAYS
            ])
            ->add('type', ChoiceType::class, [
                'invalid_message' => 'Le champs doit correspondre à un des choix',
                'choices' => Planques::PLANQUETYPE
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Planques::class,
        ]);
    }
}
