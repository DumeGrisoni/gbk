<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211004115208 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE administrateur_missions (administrateur_id INT NOT NULL, missions_id INT NOT NULL, INDEX IDX_C4C9121D7EE5403C (administrateur_id), INDEX IDX_C4C9121D17C042CF (missions_id), PRIMARY KEY(administrateur_id, missions_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrateur_agents (administrateur_id INT NOT NULL, agents_id INT NOT NULL, INDEX IDX_188ACB857EE5403C (administrateur_id), INDEX IDX_188ACB85709770DC (agents_id), PRIMARY KEY(administrateur_id, agents_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrateur_cibles (administrateur_id INT NOT NULL, cibles_id INT NOT NULL, INDEX IDX_27F81B287EE5403C (administrateur_id), INDEX IDX_27F81B289E046BDF (cibles_id), PRIMARY KEY(administrateur_id, cibles_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrateur_contacts (administrateur_id INT NOT NULL, contacts_id INT NOT NULL, INDEX IDX_C378D3107EE5403C (administrateur_id), INDEX IDX_C378D310719FB48E (contacts_id), PRIMARY KEY(administrateur_id, contacts_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrateur_planques (administrateur_id INT NOT NULL, planques_id INT NOT NULL, INDEX IDX_C0C969FE7EE5403C (administrateur_id), INDEX IDX_C0C969FE70AF8C0F (planques_id), PRIMARY KEY(administrateur_id, planques_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE administrateur_missions ADD CONSTRAINT FK_C4C9121D7EE5403C FOREIGN KEY (administrateur_id) REFERENCES administrateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_missions ADD CONSTRAINT FK_C4C9121D17C042CF FOREIGN KEY (missions_id) REFERENCES missions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_agents ADD CONSTRAINT FK_188ACB857EE5403C FOREIGN KEY (administrateur_id) REFERENCES administrateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_agents ADD CONSTRAINT FK_188ACB85709770DC FOREIGN KEY (agents_id) REFERENCES agents (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_cibles ADD CONSTRAINT FK_27F81B287EE5403C FOREIGN KEY (administrateur_id) REFERENCES administrateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_cibles ADD CONSTRAINT FK_27F81B289E046BDF FOREIGN KEY (cibles_id) REFERENCES cibles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_contacts ADD CONSTRAINT FK_C378D3107EE5403C FOREIGN KEY (administrateur_id) REFERENCES administrateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_contacts ADD CONSTRAINT FK_C378D310719FB48E FOREIGN KEY (contacts_id) REFERENCES contacts (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_planques ADD CONSTRAINT FK_C0C969FE7EE5403C FOREIGN KEY (administrateur_id) REFERENCES administrateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrateur_planques ADD CONSTRAINT FK_C0C969FE70AF8C0F FOREIGN KEY (planques_id) REFERENCES planques (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE administrateur_missions');
        $this->addSql('DROP TABLE administrateur_agents');
        $this->addSql('DROP TABLE administrateur_cibles');
        $this->addSql('DROP TABLE administrateur_contacts');
        $this->addSql('DROP TABLE administrateur_planques');
    }
}
