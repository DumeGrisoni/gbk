<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211010140755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrateur_missions ADD CONSTRAINT FK_C4C9121D17C042CF FOREIGN KEY (missions_id) REFERENCES missions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE agents ADD CONSTRAINT FK_9596AB6E17C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('ALTER TABLE cibles ADD CONSTRAINT FK_AAE47BC317C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_3340157317C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('ALTER TABLE planques ADD CONSTRAINT FK_30F1AF9D17C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrateur_missions DROP FOREIGN KEY FK_C4C9121D17C042CF');
        $this->addSql('ALTER TABLE agents DROP FOREIGN KEY FK_9596AB6E17C042CF');
        $this->addSql('ALTER TABLE cibles DROP FOREIGN KEY FK_AAE47BC317C042CF');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_3340157317C042CF');
        $this->addSql('ALTER TABLE planques DROP FOREIGN KEY FK_30F1AF9D17C042CF');
    }
}
