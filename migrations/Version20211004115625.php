<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211004115625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agents ADD missions_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE agents ADD CONSTRAINT FK_9596AB6E17C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('CREATE INDEX IDX_9596AB6E17C042CF ON agents (missions_id)');
        $this->addSql('ALTER TABLE cibles ADD missions_id INT NOT NULL');
        $this->addSql('ALTER TABLE cibles ADD CONSTRAINT FK_AAE47BC317C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('CREATE INDEX IDX_AAE47BC317C042CF ON cibles (missions_id)');
        $this->addSql('ALTER TABLE contacts ADD missions_id INT NOT NULL');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_3340157317C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('CREATE INDEX IDX_3340157317C042CF ON contacts (missions_id)');
        $this->addSql('ALTER TABLE planques ADD missions_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE planques ADD CONSTRAINT FK_30F1AF9D17C042CF FOREIGN KEY (missions_id) REFERENCES missions (id)');
        $this->addSql('CREATE INDEX IDX_30F1AF9D17C042CF ON planques (missions_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agents DROP FOREIGN KEY FK_9596AB6E17C042CF');
        $this->addSql('DROP INDEX IDX_9596AB6E17C042CF ON agents');
        $this->addSql('ALTER TABLE agents DROP missions_id');
        $this->addSql('ALTER TABLE cibles DROP FOREIGN KEY FK_AAE47BC317C042CF');
        $this->addSql('DROP INDEX IDX_AAE47BC317C042CF ON cibles');
        $this->addSql('ALTER TABLE cibles DROP missions_id');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_3340157317C042CF');
        $this->addSql('DROP INDEX IDX_3340157317C042CF ON contacts');
        $this->addSql('ALTER TABLE contacts DROP missions_id');
        $this->addSql('ALTER TABLE planques DROP FOREIGN KEY FK_30F1AF9D17C042CF');
        $this->addSql('DROP INDEX IDX_30F1AF9D17C042CF ON planques');
        $this->addSql('ALTER TABLE planques DROP missions_id');
    }
}
